# README #

## База данных и хранение данных


###  Вставка данных в коллекцию books
```js
db.books.insertMany([
  { title: "title 1", description: "description 1", authors: "authors 1" },
  { title: "title 1", description: "description 4", authors: "authors 4" },
  { title: "title 2", description: "description 2", authors: "authors 2" },
  { title: "title 3", description: "description 3", authors: "authors 3" },
])
```

###
###  Поиск полей документов коллекции books по полю title
```js
db.books.find({ title: "title 1" })
```


###
###  Поиск полей документов коллекции books по полю title
```js
db.books.updateOne({_id: ObjectId("5fde07242bc2f62a6a972652")}, {$set: {description: "description 5", authors : "authors 5"}})
db.books.updateOne({_id: ObjectId("ObjectIdBooks")}, {$set: {description: "new description", authors : "new authors"}})
```